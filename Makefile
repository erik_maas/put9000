# Very simple Makefile to compile put9000 files
#

OBJECTS = put9000.ovl put9000.com von.com voff.com

all: $(OBJECTS)

%.com: %.gen
	z80asm -o $@ $<

put9000.ovl: ansi9000.gen
	z80asm -o $@ $<

clean:
	rm $(OBJECTS)

.PHONY: test_msxdos
test_msxdos: $(OBJECTS)
	cp *.com test_msxdos
	cp *.ovl test_msxdos
	openmsx -machine "turbor" -ext gfx9000 -diska test_msxdos -script setv9990.tcl &

.PHONY: test_nextor
test_nextor: $(OBJECTS)
	cp *.com test_nextor
	cp *.ovl test_nextor
	openmsx -machine "turbor" -ext gfx9000 -diska test_nextor -script setv9990.tcl nextor/Nextor-2.1-alpha2.StandaloneASCII16.ROM -romtype ascii16 &

